# APEX REST enabled SQL

<p  >
   <!--Blog  --->
   <a href="https://www.aflorestorres.com/">
    <img src="https://img.shields.io/badge/aflorestorres-ffffff?style=social&logo=blogger&logoColor=red" />
  </a>  
    <!--Youtube  --->
  <a href="https://www.youtube.com/channel/UCaSVdkZefTYPMbe-4LYK8VA">
    <img src="https://img.shields.io/badge/Angel O. Flores-ffffff?style=social&logo=youtube&logoColor=red" />
  </a>   
 
  <!--Mail  --->
  <a href="mailto:aftorres02@gmail.com">
    <img src="https://img.shields.io/badge/-aftorres02@gmail.com-ffffff?style=social&logo=Gmail&logoColor=red&link=mailto:aftorres02@gmail.com" />
  </a>
  
  <!--linkedin  --->
  <a href="https://www.linkedin.com/in/angel-o-flores-torres-13118088/">
    <img src="https://img.shields.io/badge/Angel O. Flores -ffffff?style=social&logo=Linkedin&logoColor=blue" />
  </a>
 
  <!--Twiter  --->
  <a href="https://twitter.com/AFloresTorres10">
    <img src="https://img.shields.io/badge/@AFloresTorres10-ffffff?style=social&logo=twitter" />
  </a>
 
  <!--gitlab  --->
  <a href="https://gitlab.com/public-repo1">
    <img src="https://img.shields.io/badge/@aflorestorres-ffffff?style=social&logo=gitlab" />
  </a> 
  
</p>

## Presentación realizada para Oracle Office Hours Español.
Mas Detalles : 
- [Office Hours REST Enabled SQL](https://asktom.oracle.com/pls/apex/f?p=100:551::::551:P551_CLASS_ID,P551_INVITED:11734,N&cs=1E2DDF3DAB6FE6AE0065EA3869D75E4F4)
- [Slides](https://slides.com/aflorestorres/apex_rest_enabled_sql)

___

En esta sesión, Angel Flores nos guiará por los pasos necesarios para tener acceso a esquemas de base de datos remotas usando la funcionalidad REST Enabled SQL.

Los temas a tratar son:

- Requerimientos
- Instalación de ORDS en una base de datos Oracle - Que no tiene instalado APEX
- Creación y uso del Servicio REST Enabled SQL

Angel es Ingeniero de Sistemas y Oracle ACE Associate. En los últimos años ha participado activamente en la comunidad de Oracle, apoyando y gestionando el crecimiento de la comunidad APEX en Perú.
En su blog https://aflorestorres.com/ comparte su conocimiento sobre APEX y Oracle en Español.

 
