--------------- Simple post request

URL:  http://192.168.101.220:8080/ords/employees/_/sql


-- Credenciales

user: hr_emp
pass: hr_emp

-- header 
key: Content-Type 
value:  application/sql

-- body
raw: select sysdate from dual;



--------------- Insert post request


URL:  http://192.168.101.220:8080/ords/employees/_/sql

-- Credenciales

user: hr_emp
pass: hr_emp

-- header 
key: Content-Type 
value:  application/sql

-- body
raw: insert into EMP (EMPNO,ENAME,JOB) values (1,'OFFICE HOURS','ORACLE');


-- -- forzando error / commit implicito
insert into EMP (EMPNO,ENAME,JOB) values (1,'OFFICE HOURS 2','ORACLE');
insert into EMP (EMPNO,ENAME,JOB) values (2,'OFFICE HOURS 3','ORACLE');


-- forzando error
begin
insert into EMP (EMPNO,ENAME,JOB) values (1,'OFFICE HOURS 1','ORACLE');
insert into EMP (EMPNO,ENAME,JOB) values (2,'OFFICE HOURS 3','ORACLE');
end;



--------------- Creando un procedimiento