create or replace package apis as  
  /* todo enter package declarations (types, exceptions, methods etc) here */ 
------------------------------------  
function saludo(nombre in varchar2)
return varchar2;

END APIS;
/
create or replace package body apis as

------------------------------------
function saludo(nombre in varchar2)
return varchar2 as
begin 
    return 'Hola ' || nombre || ' hoy es: ' || to_char(sysdate,' DD MONTH, YYYY  HH:SS:AM') ;
end saludo; 

end apis;





-----------------------------

select  apis.saludo('Angel Flores')  from dual;